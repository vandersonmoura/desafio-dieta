package br.desafio.dieta.controller;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.model.chart.Axis;
import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.CategoryAxis;
import org.primefaces.model.chart.ChartSeries;
import org.primefaces.model.chart.LineChartModel;

import br.desafio.dieta.facade.UsuarioDietaFacade;
import br.desafio.dieta.model.RegistroPeso;
import br.desafio.dieta.model.UsuarioDieta;

@Named
@ViewScoped
public class DashboardBean implements Serializable {

	/**
	 * 
	 */
	@Inject private LoginBean loginBean;
	@Inject private UsuarioDietaFacade usuarioDietaFacade;
	private static final long serialVersionUID = 1L;
	private LineChartModel grafico;
	private UsuarioDieta usuarioDieta;

	@PostConstruct
	public void init() {
		this.usuarioDieta = usuarioDietaFacade.buscarUsuarioDieta(loginBean.getUsuarioLogado());
		inicializarGrafico();
	}
	
	private void inicializarGrafico() {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	    this.grafico = new LineChartModel();
		ChartSeries peso = new ChartSeries();
		peso.setLabel("Peso");
		
		if(this.usuarioDieta != null && this.usuarioDieta.getUsuario().getRegistrosDePeso() != null) {
			for (RegistroPeso rp : usuarioDieta.getUsuario().getRegistrosDePeso()) {
				Date date = rp.getDataDoRegsitro().getTime();
				peso.set(sdf.format(date), rp.getPeso());
			}
		}
		grafico.addSeries(peso);
         
        grafico.setTitle("Histórico de Peso");
        grafico.setLegendPosition("e");
        grafico.setShowPointLabels(true);
        grafico.getAxes().put(AxisType.X, new CategoryAxis("Data de registro"));
        Axis yAxis = grafico.getAxis(AxisType.Y);
        yAxis.setLabel("Peso");
        yAxis.setMin(0);
        yAxis.setMax(300);
    }
	
	public LineChartModel getGrafico() {
		return grafico;
	}

	public void setGrafico(LineChartModel grafico) {
		this.grafico = grafico;
	}
}
