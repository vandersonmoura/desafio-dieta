package br.desafio.dieta.controller;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import br.desafio.dieta.enums.TipoRefeicao;
import br.desafio.dieta.facade.UsuarioDietaFacade;
import br.desafio.dieta.model.Dieta;
import br.desafio.dieta.model.Refeicao;
import br.desafio.dieta.model.RegistroPeso;
import br.desafio.dieta.model.UsuarioDieta;
import br.desafio.dieta.util.MensagensManager;

@Named
@ViewScoped
public class DietaBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Inject private LoginBean loginBean;
	@Inject private UsuarioDietaFacade usuarioDietaFacade;
	
	private UsuarioDieta usuarioDieta;
	private Date dataInicio;
	private Date dataFinal;
	private Dieta dieta;
	private Refeicao refeicao;
	private double peso;
	
	@PostConstruct
	private void init() {
		this.usuarioDieta = this.usuarioDietaFacade.buscarUsuarioDieta(this.loginBean.getUsuarioLogado());
		if(usuarioDieta == null) {
			this.dieta = new Dieta();
			this.usuarioDieta = new UsuarioDieta(this.loginBean.getUsuarioLogado(), this.dieta);
		}else {
			this.dieta = usuarioDieta.getDieta();
			this.dataInicio = dieta.getDataInicio().getTime();
			this.dataFinal = dieta.getDataFinal().getTime();
			this.peso = usuarioDieta.getUsuario().getPesoMaisRecente();
		}
		this.refeicao = new Refeicao();
	}
	
	public void adicionarRefeicao() {
		if(this.refeicao.getHorario() == null || (this.refeicao.getHorario() != null && this.refeicao.getHorario().replace("_", "").replace(":", "").isEmpty()) || this.refeicao.getTipo() == null) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, MensagensManager.readMessage("horario.refeicao.nao.selecionados.erro"), ""));
		}else {
			this.refeicao.setDieta(this.dieta);
			this.dieta.getRefeicoes().add(this.refeicao);
			this.refeicao = new Refeicao();
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, MensagensManager.readMessage("refeicao.adicionada.sucesso"), ""));
		}
	}
	
	public void salvarDieta() {
		this.dieta.getDataInicio().setTime(this.dataInicio);
		this.dieta.getDataFinal().setTime(this.dataFinal);
		if(this.usuarioDieta == null) {
			this.usuarioDieta = new UsuarioDieta(this.loginBean.getUsuarioLogado(), this.dieta);
		}else {
			this.usuarioDieta.setDieta(this.dieta);
		}
		RegistroPeso registroPeso = new RegistroPeso(this.peso, Calendar.getInstance());
		registroPeso.setUsuario(this.usuarioDieta.getUsuario());
		this.usuarioDieta.getUsuario().getRegistrosDePeso().add(registroPeso);
		this.usuarioDieta =  this.usuarioDietaFacade.salvarUsuarioDieta(this.usuarioDieta);
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, MensagensManager.readMessage("dieta.salva.sucesso"), ""));
	}
	
	public Date getDataInicio() {
		return dataInicio;
	}

	public void setDataInicio(Date dataInicio) {
		this.dataInicio = dataInicio;
	}

	public Date getDataFinal() {
		return dataFinal;
	}

	public void setDataFinal(Date dataFinal) {
		this.dataFinal = dataFinal;
	}
	
	public TipoRefeicao[] tiposRefeicao() {
		return TipoRefeicao.values();
	}

	public Dieta getDieta() {
		return dieta;
	}

	public void setDieta(Dieta dieta) {
		this.dieta = dieta;
	}

	public Refeicao getRefeicao() {
		return refeicao;
	}

	public void setRefeicao(Refeicao refeicao) {
		this.refeicao = refeicao;
	}

	public UsuarioDieta getUsuarioDieta() {
		return usuarioDieta;
	}

	public void setUsuarioDieta(UsuarioDieta usuarioDieta) {
		this.usuarioDieta = usuarioDieta;
	}

	public double getPeso() {
		return peso;
	}

	public void setPeso(double peso) {
		this.peso = peso;
	}
}
