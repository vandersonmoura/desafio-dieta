package br.desafio.dieta.controller;

import java.io.IOException;
import java.io.Serializable;
import java.util.Map;

import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;

import br.desafio.dieta.facade.UsuarioFacade;
import br.desafio.dieta.model.Usuario;
import br.desafio.dieta.util.MD5Util;
import br.desafio.dieta.util.MensagensManager;

@Named
@SessionScoped
public class LoginBean implements Serializable{
	private static final long serialVersionUID = 1L;

	@Inject
	private UsuarioFacade usuarioFacade;
	private String login;
	private String senha;

	public void fazerLogin() {
		FacesContext facesContext = FacesContext.getCurrentInstance();
		if (getUsuarioLogado() == null) {
			String senhaCriptografada = MD5Util.gerarMD5(senha);
			Usuario usuario = usuarioFacade.buscarUsuario(login, senhaCriptografada);
			if (usuario != null) {
				Map<String, Object> session = facesContext.getExternalContext().getSessionMap();
				session.put("usuarioLogado", usuario);
				try {
					facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, MensagensManager.readMessage("boas.vindas"), MensagensManager.readMessage("login.sucesso")));
					facesContext.getExternalContext().getFlash().setKeepMessages(true);
					facesContext.getExternalContext().redirect("home.xhtml");
				} catch (IOException e) {
					e.printStackTrace();
				}
			} else {
				facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, MensagensManager.readMessage("login.erro"), MensagensManager.readMessage("login.erro.detalhe")));
			}
		}
	}

	public void fazerLogout() {
		Map<String, Object> session = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
		session.remove("usuarioLogado");
		HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
		request.getSession().invalidate();
		try {
			FacesContext.getCurrentInstance().getExternalContext().redirect("login.xhtml");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public Usuario getUsuarioLogado() {
		Map<String, Object> session = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
		return (Usuario) session.get("usuarioLogado");
	}
	
	public void irParaCadastroDeUsuario() {
		try {
			FacesContext.getCurrentInstance().getExternalContext().redirect("usuario/cadastro.xhtml");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}
}
