package br.desafio.dieta.controller;

import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import br.desafio.dieta.facade.UsuarioFacade;
import br.desafio.dieta.model.Usuario;
import br.desafio.dieta.util.MD5Util;

@Named
@RequestScoped
public class UsuarioBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private UsuarioFacade usuarioFacade;
	private Usuario usuario;

	@PostConstruct
	private void init() {
		this.usuario = new Usuario();
	}

	public void adicionarUsuario() {
		String senhaCriptografada = MD5Util.gerarMD5(this.usuario.getSenha());
		this.usuario.setSenha(senhaCriptografada);
		try {
			this.usuarioFacade.salvarUsuario(usuario);
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Cadastro realizado!", "Usuário cadastrado com sucesso!"));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
			FacesContext.getCurrentInstance().getExternalContext().redirect("../login.xhtml");
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Erro durante o cadastro!", e.getMessage()));
		}
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
}
