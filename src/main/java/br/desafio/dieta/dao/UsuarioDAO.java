package br.desafio.dieta.dao;

import javax.ejb.Stateless;

import com.uaihebert.factory.EasyCriteriaFactory;
import com.uaihebert.model.EasyCriteria;

import br.desafio.dieta.dao.common.GenericDAO;
import br.desafio.dieta.model.Usuario;

@SuppressWarnings("serial")
@Stateless
public class UsuarioDAO extends GenericDAO<Usuario> {

	
	public UsuarioDAO() {
		super(Usuario.class);
	}

	public Usuario buscarUsuario(String login, String senha) {
		try {
			EasyCriteria<Usuario> criteria = EasyCriteriaFactory.createQueryCriteria(em, Usuario.class);
			criteria.andEquals("login", login);
			if(senha != null) {
				criteria.andEquals("senha", senha);
			}
			return criteria.getSingleResult();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public void salvarUsuario(Usuario usuario) {
		this.save(usuario);
	}
}
