package br.desafio.dieta.dao;

import javax.ejb.Stateless;

import com.uaihebert.factory.EasyCriteriaFactory;
import com.uaihebert.model.EasyCriteria;

import br.desafio.dieta.dao.common.GenericDAO;
import br.desafio.dieta.model.Usuario;
import br.desafio.dieta.model.UsuarioDieta;

@SuppressWarnings("serial")
@Stateless
public class UsuarioDietaDAO extends GenericDAO<UsuarioDieta>{
	
	public UsuarioDietaDAO() {
		super(UsuarioDieta.class);
	}
	
	public UsuarioDieta buscarUsuarioDieta(Usuario usuario) {
		try {
			EasyCriteria<UsuarioDieta> criteria = EasyCriteriaFactory.createQueryCriteria(em, UsuarioDieta.class);
			criteria.andEquals("usuario", usuario);
			return criteria.getSingleResult();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public UsuarioDieta salvarUsuarioDieta(UsuarioDieta usuarioDieta) {
		return this.save(usuarioDieta);
	}
}
