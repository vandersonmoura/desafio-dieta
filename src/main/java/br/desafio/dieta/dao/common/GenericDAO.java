package br.desafio.dieta.dao.common;

import java.io.Serializable;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;


public abstract class GenericDAO<T extends PersistenceEntity> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final String UNIT_NAME = "DietaPU";


	private Class<T> entityClass;
	
	@PersistenceContext(unitName = UNIT_NAME)
	protected EntityManager em; 

	public GenericDAO(Class<T> entityClass) {
		this.entityClass = entityClass;
	}
	
	public EntityManager getEntityManager() {
		return em;
	}

	public void setEntityManager(EntityManager em) {
		this.em = em;
	}

	public T save(T entity) {
		
		if (entity.getId() != null && entity.getId() == 0) {
			entity.setId(null);
		}

		entity = em.merge(entity);
		return entity;
	}

	public final void delete(T object) {
		em.remove(object);
	}

	@SuppressWarnings("unchecked")
	public List<T> findAll() {
		Query query = em.createQuery("from " + entityClass.getSimpleName());
		List<T> entities = query.getResultList();
		return entities;
	}

	public T findById(Long id) {
		T result = null;
		try {
			result = em.find(entityClass, id);
		} catch (RuntimeException runtimeEx) {
			runtimeEx.printStackTrace();
		}
		return result;
	}

	public Class<T> getEntityClass() {
		return entityClass;
	}
}
