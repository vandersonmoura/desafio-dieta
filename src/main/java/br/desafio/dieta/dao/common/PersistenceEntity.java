package br.desafio.dieta.dao.common;

import java.io.Serializable;

public interface PersistenceEntity extends Serializable {
	Long getId();
	void setId(Long id);
}
