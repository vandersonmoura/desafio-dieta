package br.desafio.dieta.enums;

public enum TipoRefeicao {
	CAFE_DA_MANHA("Café da Manhã"),
	LANCHE("Lanche"),
	ALMOCO("Almoço"),
	JANTAR("Jantar"),
	CEIA("Ceia");
	
	private String nome;
	
	private TipoRefeicao(String nome) {
		this.nome = nome;
	}
	
	public String getNome() {
		return nome;
	}
}
