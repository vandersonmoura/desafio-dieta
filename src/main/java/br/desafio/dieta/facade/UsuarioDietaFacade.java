package br.desafio.dieta.facade;

import java.io.Serializable;

import javax.ejb.Stateless;
import javax.inject.Inject;

import br.desafio.dieta.dao.UsuarioDietaDAO;
import br.desafio.dieta.model.Usuario;
import br.desafio.dieta.model.UsuarioDieta;

@Stateless
public class UsuarioDietaFacade implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Inject
	private UsuarioDietaDAO usuarioDietaDAO;
	
	public UsuarioDieta buscarUsuarioDieta(Usuario usuario) {
		return usuarioDietaDAO.buscarUsuarioDieta(usuario);
	}
	
	public UsuarioDieta salvarUsuarioDieta(UsuarioDieta usuarioDieta) {
		return this.usuarioDietaDAO.salvarUsuarioDieta(usuarioDieta);
	}
}
