package br.desafio.dieta.facade;

import java.io.Serializable;

import javax.ejb.Stateless;
import javax.inject.Inject;

import br.desafio.dieta.dao.UsuarioDAO;
import br.desafio.dieta.model.Usuario;
import br.desafio.dieta.util.MensagensManager;

@Stateless
public class UsuarioFacade implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Inject
	private UsuarioDAO usuarioDAO;

	public UsuarioDAO getUsuarioDAO() {
		return usuarioDAO;
	}

	public void setUsuarioDAO(UsuarioDAO usuarioDAO) {
		this.usuarioDAO = usuarioDAO;
	}
	
	public Usuario buscarUsuario(String login, String senha) {
		return this.usuarioDAO.buscarUsuario(login, senha);
	}
	
	public void salvarUsuario(Usuario usuario) throws Exception {
		verificarSeUsuarioExiste(usuario.getLogin());
		this.usuarioDAO.salvarUsuario(usuario);
	}
	
	private void verificarSeUsuarioExiste(String login) throws Exception {
		Usuario usuario = usuarioDAO.buscarUsuario(login, null);
		if(usuario != null) {
			throw new Exception(MensagensManager.readMessage("usuario.ja.cadastrado.erro"));
		}
	}
}
