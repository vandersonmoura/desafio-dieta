package br.desafio.dieta.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.desafio.dieta.model.Usuario;


@WebFilter(urlPatterns = { "/*" })
public class PageFilter implements Filter {

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest httpRequest = (HttpServletRequest) request;
		Usuario usuario = (Usuario) httpRequest.getSession().getAttribute("usuarioLogado");

		String paginaAcessada = httpRequest.getRequestURI();
		boolean requestDoLogin = paginaAcessada.contains("login.xhtml");

		if (usuario != null) {
			if (requestDoLogin) {
				HttpServletResponse httpResponse = (HttpServletResponse) response;
				httpResponse.sendRedirect("home.xhtml");
			} else {
				chain.doFilter(request, response);
			}
		} else {
			if (!requestDoLogin && !paginaAcessada.contains("javax.faces.resource") && !paginaAcessada.contains("/usuario/cadastro.xhtml")) {
				httpRequest.getRequestDispatcher("/login.xhtml").forward(
						request, response);
			} else {
				chain.doFilter(request, response);
			}
		}
	}

	@Override
	public void destroy() {
	}
}
