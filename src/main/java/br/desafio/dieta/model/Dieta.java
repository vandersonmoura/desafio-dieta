package br.desafio.dieta.model;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import br.desafio.dieta.dao.common.PersistenceEntity;


@Entity
public class Dieta implements PersistenceEntity{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	private Calendar dataInicio;
	private Calendar dataFinal;
	
	@OneToMany(mappedBy="dieta", targetEntity = Refeicao.class, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
	@LazyCollection(LazyCollectionOption.FALSE)
	private List<Refeicao> refeicoes;
	
	public Dieta() {
		super();
		this.refeicoes = new ArrayList<>();
		this.dataInicio = Calendar.getInstance();
		this.dataFinal = Calendar.getInstance();
	}
	
	public Dieta(Calendar dataInicio, Calendar dataFinal, List<Refeicao> refeicoes) {
		this.dataInicio = dataInicio;
		this.dataFinal = dataFinal;
		this.refeicoes = refeicoes;
	}
	
	@Override
	public Long getId() {
		return id;
	}
	
	@Override
	public void setId(Long id) {
		this.id = id;
	}
	
	public Calendar getDataInicio() {
		return dataInicio;
	}
	
	public void setDataInicio(Calendar dataInicio) {
		this.dataInicio = dataInicio;
	}
	
	public Calendar getDataFinal() {
		return dataFinal;
	}
	
	public void setDataFinal(Calendar dataFinal) {
		this.dataFinal = dataFinal;
	}
	
	public List<Refeicao> getRefeicoes() {
		return refeicoes;
	}
	
	public void setRefeicoes(List<Refeicao> refeicoes) {
		this.refeicoes = refeicoes;
	}
	
}
