package br.desafio.dieta.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import br.desafio.dieta.dao.common.PersistenceEntity;
import br.desafio.dieta.enums.TipoRefeicao;

@Entity
public class Refeicao implements PersistenceEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	private TipoRefeicao tipo;
	private String horario;
	private String descricao;
	
	@ManyToOne
	private Dieta dieta;
	
	@Override
	public Long getId() {
		return id;
	}
	
	@Override
	public void setId(Long id) {
		this.id = id;
	}
	
	public TipoRefeicao getTipo() {
		return tipo;
	}
	
	public void setTipo(TipoRefeicao tipo) {
		this.tipo = tipo;
	}
	
	public String getHorario() {
		return horario;
	}
	
	public void setHorario(String horario) {
		this.horario = horario;
	}
	
	public String getDescricao() {
		return descricao;
	}
	
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Dieta getDieta() {
		return dieta;
	}

	public void setDieta(Dieta dieta) {
		this.dieta = dieta;
	} 
}
