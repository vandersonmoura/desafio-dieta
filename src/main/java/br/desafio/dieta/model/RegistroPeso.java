package br.desafio.dieta.model;

import java.io.Serializable;
import java.util.Calendar;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import br.desafio.dieta.dao.common.PersistenceEntity;

@Entity 
public class RegistroPeso implements Serializable, PersistenceEntity{

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	private double peso;
	private Calendar dataDoRegsitro;

	@ManyToOne
	private Usuario usuario;
	
	public RegistroPeso() {
		super();
		this.dataDoRegsitro = Calendar.getInstance();
	}

	public RegistroPeso(double peso, Calendar dataDoRegsitro) {
		super();
		this.peso = peso;
		this.dataDoRegsitro = dataDoRegsitro;
	}

	@Override
	public Long getId() {
		return id;
	}
	
	@Override
	public void setId(Long id) {
		this.id = id;
	}
	
	public double getPeso() {
		return peso;
	}

	public void setPeso(double peso) {
		this.peso = peso;
	}

	public Calendar getDataDoRegsitro() {
		return dataDoRegsitro;
	}

	public void setDataDoRegsitro(Calendar dataDoRegsitro) {
		this.dataDoRegsitro = dataDoRegsitro;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

}
