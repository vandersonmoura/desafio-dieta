package br.desafio.dieta.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import br.desafio.dieta.dao.common.PersistenceEntity;

@Entity
public class Usuario implements PersistenceEntity {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	private String nome;
	private String login;
	private String senha;
	
	@OneToMany(mappedBy="usuario", targetEntity = RegistroPeso.class, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
	@LazyCollection(LazyCollectionOption.FALSE)
	private List<RegistroPeso> registrosDePeso;
	
	private double pesoIdeal;
	private double altura;
	
	public Usuario() {
		super();
		this.registrosDePeso = new ArrayList<>();
	}
	
	public double getPesoMaisRecente() {
		return this.registrosDePeso.get(this.registrosDePeso.size()-1).getPeso();
	}
	
	@Override
	public Long getId() {
		return id;
	}
	
	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public String getLogin() {
		return login;
	}
	
	public void setLogin(String login) {
		this.login = login;
	}
	
	public String getSenha() {
		return senha;
	}
	
	public void setSenha(String senha) {
		this.senha = senha;
	}

	public double getPesoIdeal() {
		return pesoIdeal;
	}

	public void setPesoIdeal(double pesoIdeal) {
		this.pesoIdeal = pesoIdeal;
	}

	public double getAltura() {
		return altura;
	}

	public void setAltura(double altura) {
		this.altura = altura;
	}

	public List<RegistroPeso> getRegistrosDePeso() {
		return registrosDePeso;
	}

	public void setRegistrosDePeso(List<RegistroPeso> registrosDePeso) {
		this.registrosDePeso = registrosDePeso;
	}
}
