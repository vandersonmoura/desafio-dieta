package br.desafio.dieta.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import br.desafio.dieta.dao.common.PersistenceEntity;

@Entity
public class UsuarioDieta implements PersistenceEntity{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@OneToOne (cascade= {CascadeType.MERGE, CascadeType.PERSIST})
	private Usuario usuario;
	
	@OneToOne (cascade= {CascadeType.MERGE, CascadeType.PERSIST})
	private Dieta dieta;
	
	public UsuarioDieta() {
		super();
	}
	
	public UsuarioDieta(Usuario usuario, Dieta dieta) {
		super();
		this.usuario = usuario;
		this.dieta = dieta;
	}
	
	@Override
	public Long getId() {
		return id;
	}
	
	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Dieta getDieta() {
		return dieta;
	}

	public void setDieta(Dieta dieta) {
		this.dieta = dieta;
	}
}
