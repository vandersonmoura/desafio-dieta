package br.desafio.dieta.util;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class MD5Util {
	public static String gerarMD5(String valor) {
		MessageDigest md;
		try {
			md = MessageDigest.getInstance("MD5");
			md.update(valor.getBytes(), 0, valor.length());
			return new BigInteger(1, md.digest()).toString(16);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			return null;
		}
	} 
}
