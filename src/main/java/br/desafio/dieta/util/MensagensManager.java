package br.desafio.dieta.util;

import java.text.MessageFormat;
import java.util.Locale;
import java.util.ResourceBundle;

public class MensagensManager {
	
	/**
	 * Esse método recebe a chave que se encontra no arquivo mensagens.properties e retorna o seu conteúdo.
	 * 
	 * @param key chave que se deseja obter o conteúdo
	 * @return texto que se encontra no arquivo mensagens.properties
	 */
	public static String readMessage(String key, Object... params) {
		ResourceBundle bundle = ResourceBundle.getBundle("mensagens", new Locale("pt_br"));
		return MessageFormat.format(bundle.getString(key), params); 
	}
}
