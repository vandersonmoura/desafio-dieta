package br.desafio.dieta.dao;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import br.desafio.dieta.model.Usuario;
import br.desafio.dieta.dao.builder.UsuarioBuilder;
import br.desafio.dieta.util.DatabaseUtil;

public class UsuarioDAOTest {
	UsuarioDAO usuarioDAO = null;
	
	@BeforeClass
	public static void beforeClass(){
		DatabaseUtil.INSTANCE.reset();
	}
	
	@Before
	public void setUp(){
		usuarioDAO = DatabaseUtil.INSTANCE.buidDAO(UsuarioDAO.class);
		usuarioDAO.getEntityManager().getTransaction().begin();
	}

	@After
	public void tearDown(){
		usuarioDAO.getEntityManager().getTransaction().rollback();
	}
	
	@Test
	public void buscarUsuarioInexistentePorLoginTest(){
		Usuario usuario = usuarioDAO.buscarUsuario("login.inexistente", null);
		Assert.assertNull(usuario);
	}
	
	@Test
	public void buscarUsuarioInexistentePorLoginESenhaTest(){
		Usuario usuario = usuarioDAO.buscarUsuario("login.inexistente", "senha.inexistente");
		Assert.assertNull(usuario);
	}
	
	@Test
	public void buscarUsuarioExistentePorLoginTest(){
		new UsuarioBuilder().random().withLogin("login.existente").buildPersistent(usuarioDAO.getEntityManager());
		Usuario usuario = usuarioDAO.buscarUsuario("login.existente", null);
		Assert.assertNotNull(usuario);
	}
	
	@Test
	public void buscarUsuarioExistentePorLoginESenhaTest(){
		new UsuarioBuilder().random().withLogin("login.existente").withSenha("senha.existente").buildPersistent(usuarioDAO.getEntityManager());
		Usuario usuario = usuarioDAO.buscarUsuario("login.existente", "senha.existente");
		Assert.assertNotNull(usuario);
	}
}

