package br.desafio.dieta.dao;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import br.desafio.dieta.dao.builder.UsuarioDietaBuilder;
import br.desafio.dieta.model.Usuario;
import br.desafio.dieta.model.UsuarioDieta;
import br.desafio.dieta.util.DatabaseUtil;

public class UsuarioDietaDAOTest {
	UsuarioDietaDAO usuarioDietaDAO = null;
	
	@BeforeClass
	public static void beforeClass(){
		DatabaseUtil.INSTANCE.reset();
	}
	
	@Before
	public void setUp(){
		usuarioDietaDAO = DatabaseUtil.INSTANCE.buidDAO(UsuarioDietaDAO.class);
		usuarioDietaDAO.getEntityManager().getTransaction().begin();
	}

	@After
	public void tearDown(){
		usuarioDietaDAO.getEntityManager().getTransaction().rollback();
	}
	
	@Test
	public void buscarUsuarioDietaPorUusarioExistente(){
		Usuario usuario = new Usuario();
		UsuarioDieta ud = new UsuarioDietaBuilder().random().withUsuario(usuario).buildPersistent(usuarioDietaDAO.getEntityManager());
		usuarioDietaDAO.buscarUsuarioDieta(ud.getUsuario());
		Assert.assertNotNull(usuarioDietaDAO.buscarUsuarioDieta(ud.getUsuario()));
	}
	
	@Test
	public void buscarUsuarioDietaPorUusarioInexistente(){
		UsuarioDieta ud = new UsuarioDietaBuilder().random().buildPersistent(usuarioDietaDAO.getEntityManager());
		usuarioDietaDAO.buscarUsuarioDieta(new Usuario());
		Assert.assertNull(usuarioDietaDAO.buscarUsuarioDieta(ud.getUsuario()));
	}
	
}
