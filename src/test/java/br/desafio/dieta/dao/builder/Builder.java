package br.desafio.dieta.dao.builder;

import javax.persistence.EntityManager;

public abstract class Builder<T, B extends Builder<T,B>> {

	T instance;
	EntityManager em = null;

	protected abstract Class<T> getKlass();
	
	public Builder() {
		try {
			instance = (T) getKlass().newInstance();
			init();
		} catch (InstantiationException | IllegalAccessException e) {
			e.printStackTrace();
		}
	}
	
	protected void init() {
	}

	@SuppressWarnings("unchecked")
	public B persistent(EntityManager em){
		this.em = em;
		return ((B) this);
	}

	@SuppressWarnings("unchecked")
	public B random(){
		RandomObjectFiller randomizer = new RandomObjectFiller();
		try {
			this.instance = (T) randomizer.createAndFill(getKlass());
		} catch (Exception e) {
		}
		return (B) this;
	}

	public T buildPersistent(EntityManager em){
		if(em!=null){
			em.persist(instance);
		}
		return instance;
	}

	public T build(){
		return instance;
	}

	protected T getInstance() {
		return instance;
	}
	
}
