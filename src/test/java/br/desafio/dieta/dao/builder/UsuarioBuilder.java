package br.desafio.dieta.dao.builder;

import br.desafio.dieta.model.Usuario;


public class UsuarioBuilder extends Builder<Usuario, UsuarioBuilder>{

	@Override
	protected Class<Usuario> getKlass() {
		return Usuario.class;
	}
	
	public UsuarioBuilder withLogin(String login){
		getInstance().setLogin(login);
		return this;
	}
	
	public UsuarioBuilder withSenha(String senha){
		getInstance().setSenha(senha);
		return this;
	}
}
