package br.desafio.dieta.dao.builder;

import br.desafio.dieta.model.Usuario;
import br.desafio.dieta.model.UsuarioDieta;

public class UsuarioDietaBuilder extends Builder<UsuarioDieta, UsuarioDietaBuilder>{

	@Override
	protected Class<UsuarioDieta> getKlass() {
		return UsuarioDieta.class;
	}
	
	public UsuarioDietaBuilder withUsuario(Usuario usuario){
		getInstance().setUsuario(usuario);
		return this;
	}
}
