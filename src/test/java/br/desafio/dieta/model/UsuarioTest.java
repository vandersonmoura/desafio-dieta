package br.desafio.dieta.model;

import java.util.Calendar;

import org.junit.Assert;
import org.junit.Test;


public class UsuarioTest {
	
	@Test
	public void obterUltimoRegistroDePesoDoUsuarioTest(){
		RegistroPeso rp1 = new RegistroPeso(90.0D, Calendar.getInstance());
		RegistroPeso rp2 = new RegistroPeso(85.0D, Calendar.getInstance());
		Usuario usuario = new Usuario();
		usuario.getRegistrosDePeso().add(rp1);
		usuario.getRegistrosDePeso().add(rp2);
		Assert.assertEquals(0, Double.compare(rp2.getPeso(), usuario.getPesoMaisRecente()));
	}
}
