package br.desafio.dieta.util;
import java.util.EnumSet;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.hibernate.boot.spi.MetadataImplementor;
import org.hibernate.jpa.HibernatePersistenceProvider;
import org.hibernate.jpa.boot.internal.EntityManagerFactoryBuilderImpl;
import org.hibernate.jpa.boot.spi.EntityManagerFactoryBuilder;
import org.hibernate.tool.hbm2ddl.SchemaExport;
import org.hibernate.tool.hbm2ddl.SchemaExport.Action;
import org.hibernate.tool.schema.TargetType;

import br.desafio.dieta.dao.common.GenericDAO;

public class DatabaseUtil {

	public static DatabaseUtil INSTANCE = new DatabaseUtil();
	String persistenceUnitName = "mnf-pu-test";

	private EntityManagerFactory factory;
	
	public DatabaseUtil() {
		factory= Persistence.createEntityManagerFactory(persistenceUnitName);
	}
	
	public DatabaseUtil instance(){
		return INSTANCE;
	}
	
	private class CustomHibernatePersistenceProvider extends HibernatePersistenceProvider{
		public EntityManagerFactoryBuilder getEntityManagerFactoryBuilder(String persistenceUnitName, Map<String, Object> properties) {
			return getEntityManagerFactoryBuilderOrNull(persistenceUnitName, properties);
		}
	}
	
	public <T extends GenericDAO<?>> T buidDAO(Class<T> klass){
		T dao = null;
		try {
			dao = klass.newInstance();
			EntityManager em = factory.createEntityManager();
			dao.setEntityManager(em);
		} catch (InstantiationException | IllegalAccessException e) {
			e.printStackTrace();
		}
		return dao;
	}

	public void reset(){
		@SuppressWarnings("rawtypes")
		Map properties = factory.getProperties();
		@SuppressWarnings("unchecked")
		EntityManagerFactoryBuilderImpl builder = (EntityManagerFactoryBuilderImpl) new CustomHibernatePersistenceProvider().getEntityManagerFactoryBuilder("mnf-pu-test", properties); 
		builder.build();
		MetadataImplementor metadata = builder.getMetadata();
		SchemaExport schemaExport = new SchemaExport();
		schemaExport.setHaltOnError(true);
		schemaExport.execute(EnumSet.of(TargetType.DATABASE), Action.BOTH, metadata);
	}
}
